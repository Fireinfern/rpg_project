using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public event EventHandler OnHealthChanged;
    public event EventHandler OnDead;

    public delegate void OnHealedDelegate();

    public OnHealedDelegate OnHealed;

    private int healthMax = 100;
    private int health = 100;

    public HealthSystem(int newHealthMax)
    {
        healthMax = newHealthMax;
        health = healthMax;
    }

    public void SetMaxHealth(int newHealthMax)
    {
        healthMax = newHealthMax;
        health = healthMax;
    }

    public void SetHealthAmount(int newHealth)
    {
        health = newHealth;
        if (OnHealthChanged != null) OnHealthChanged(this, EventArgs.Empty);
    }

    public float GetHealthPercent()
    {
        return (float)health / healthMax;
    }

    public int GetHealthAmount()
    {
        return health;
    }

    public void Damage(int amount)
    {
        health -= amount;
        if (health < 0)
        {
            health = 0;
        }
        if (OnHealthChanged != null) OnHealthChanged(this, EventArgs.Empty);

        if (health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        if (OnDead != null) OnDead(this, EventArgs.Empty);
    }

    public bool IsDead()
    {
        return health <= 0;
    }

    public void Heal(int amount)
    {
        health += amount;
        if (health > healthMax)
        {
            health = healthMax;
        }
        if (OnHealthChanged != null) OnHealthChanged(this, EventArgs.Empty);
        OnHealed.Invoke();
    }
}
