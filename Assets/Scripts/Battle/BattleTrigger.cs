﻿using System;
using UnityEngine;

namespace Battle
{
    public class BattleTrigger : MonoBehaviour
    {
        [SerializeField] private BattleManager battleManager;
        [SerializeField] private int qtyEnemies = 1;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            
            battleManager.StartBattle(qtyEnemies);
            
            Destroy(gameObject);
        }
    }
}