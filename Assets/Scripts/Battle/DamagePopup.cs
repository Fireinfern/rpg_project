using System;
using TMPro;
using UnityEngine;

namespace Battle
{
    public class DamagePopup : MonoBehaviour
    {
        public static DamagePopup Create(Vector3 position, int damageAmount)
        {
            var damagePopupTransform = Instantiate(GameAssets.instance.damagePopupPrefab, position, Quaternion.identity);
            var damagePopup = damagePopupTransform.GetComponent<DamagePopup>();
            damagePopup.Setup(damageAmount);
            
            return damagePopup;
        }

        private const float DisappearTimerMax = 3f;
        
        private TextMeshPro _textMeshPro;
        private float _disappearTimer;
        private Color _textColor;
        
        private void Awake()
        {
            _textMeshPro = transform.GetComponent<TextMeshPro>();
        }

        private void Setup(int damageAmount)
        {
            _textMeshPro.SetText(damageAmount.ToString());
            _textColor = _textMeshPro.color;
            _disappearTimer = DisappearTimerMax;
        }

        private void Update()
        {
            const float moveYSpeed = 3f;
            transform.position += new Vector3(0, moveYSpeed) * Time.deltaTime;

            _disappearTimer -= Time.deltaTime;
            if (!(_disappearTimer < 0)) return;
            const float disappearSpeed = 3f;
            _textColor.a -= disappearSpeed * Time.deltaTime;
            _textMeshPro.color = _textColor;

            if (_textColor.a < 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
