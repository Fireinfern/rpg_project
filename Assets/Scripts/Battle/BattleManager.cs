using Cinemachine;
using Movement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour
{
    [SerializeField]
    CinemachineVirtualCamera vcam;

    [SerializeField]
    GameObject player;

    [SerializeField]
    Transform enemyPrefab;

    [SerializeField]
    List<Button> playerButtons = new List<Button>();

    [SerializeField]
    GameObject battleCanvas;


    private Cinemachine3rdPersonFollow _3RdPersonFollow;
    private CharacterBattle _playerCharacterBattle;
    private List<CharacterBattle> _enemiesCharacterBattle;
    private CharacterBattle _activeCharacterBattle;
    private List<GameObject> _enemiesGameObject;

    private State state;
    private float oiginalVcamLensFov;
    private Vector3 originalVCamShoulderOffset;

    [FormerlySerializedAs("_playerSkills")] [SerializeField]
    private List<Skill> playerSkills;
    
    [FormerlySerializedAs("_enemySkills")] [SerializeField]
    private List<Skill> enemySkills;

    private enum State
    {
        WaitingForPlayer,
        Busy
    }

    // Start is called before the first frame update
    private void Start()
    {
        _3RdPersonFollow = vcam.GetCinemachineComponent<Cinemachine3rdPersonFollow>();
        _playerCharacterBattle = player.GetComponent<CharacterBattle>();
    }

    public void PlayerMove(int skillIndex)
    {
        if (skillIndex < 0 || skillIndex > playerSkills.Count)
        {
            Debug.LogError($"Skill index: {skillIndex} not found");
        }

        var skill = playerSkills[skillIndex];
        if (state != State.WaitingForPlayer) return;

        state = State.Busy;
        playerButtons.ForEach(button => { button.interactable = false; });
        var enemyCharacterBattle = _enemiesCharacterBattle[0];
        _playerCharacterBattle.Attack(enemyCharacterBattle, skill, () =>
        {
            ChooseNextActiveCharacter();
            playerButtons.ForEach(button => { button.interactable = true; });
        });
    }

    public void StartBattle(int qtyEnemies)
    {
        CharacterMovement.instance.inCombat = true;
        battleCanvas.SetActive(true);
        oiginalVcamLensFov = vcam.m_Lens.FieldOfView;
        originalVCamShoulderOffset = _3RdPersonFollow.ShoulderOffset;
        vcam.m_Lens.FieldOfView = 70;
        _3RdPersonFollow.ShoulderOffset.x = 2;
        _3RdPersonFollow.ShoulderOffset.y = -1;
        SpawnEnemies(qtyEnemies);
        
        SetActiveCharacterBattle(_playerCharacterBattle);
        state = State.WaitingForPlayer;
    }

    private void SpawnEnemies(int qtyEnemies)
    {
        _enemiesCharacterBattle = new List<CharacterBattle>();
        _enemiesGameObject = new List<GameObject>();
        var position = player.transform.position;
        position.x += 5;
        for (var i = 0; i < qtyEnemies; i++)
        {
            position.x += i;
            var enemy = Instantiate(enemyPrefab, position, Quaternion.identity);
            _enemiesCharacterBattle.Add(enemy.GetComponent<CharacterBattle>());
            _enemiesGameObject.Add(enemy.gameObject);
        }
    }

    private IEnumerator EndBattle()
    {
        yield return new WaitForSeconds(3);
        CharacterMovement.instance.inCombat = false;
        battleCanvas.SetActive(false);
        vcam.m_Lens.FieldOfView = oiginalVcamLensFov;
        _3RdPersonFollow.ShoulderOffset = originalVCamShoulderOffset;
        _enemiesGameObject.ForEach(Destroy);
        _playerCharacterBattle.HideSelectionCircle();
    }

    private void SetActiveCharacterBattle(CharacterBattle characterBattle)
    {
        if (_activeCharacterBattle != null)
        {
            _activeCharacterBattle.HideSelectionCircle();
        }
        _activeCharacterBattle = characterBattle;
        _activeCharacterBattle.ShowSelectionCircle();
    }

    private void ChooseNextActiveCharacter()
    {
        if (!TestBattleOver())
        {
            if (_activeCharacterBattle == _playerCharacterBattle)
            {
                // enemies turn
                var enemyCharacterBattle = _enemiesCharacterBattle[0];
                StartCoroutine(EnemyTurn(enemyCharacterBattle));
            }
            else
            {
                var currentEnemyIndex = _enemiesCharacterBattle.IndexOf(_activeCharacterBattle);
                var nextEnemyIndex = currentEnemyIndex + 1;
                // still enemy turn
                if (nextEnemyIndex < _enemiesCharacterBattle.Count)
                {
                    var enemyCharacterBattle = _enemiesCharacterBattle[nextEnemyIndex];
                    StartCoroutine(EnemyTurn(enemyCharacterBattle));
                }
                // player turn
                else
                {
                    SetActiveCharacterBattle(_playerCharacterBattle);
                    state = State.WaitingForPlayer;
                }
            }
        } else
        {
            StartCoroutine(EndBattle());
        }
    }

    private IEnumerator EnemyTurn(CharacterBattle enemyCharacterBattle)
    {
        SetActiveCharacterBattle(enemyCharacterBattle);
        state = State.Busy;
        yield return new WaitForSeconds(1);
        var skill = GetRandomSkillForEnemy();
        enemyCharacterBattle.Attack(_playerCharacterBattle, skill, ChooseNextActiveCharacter);
    }

    private Skill GetRandomSkillForEnemy()
    {
        var rand = new System.Random();

        var randomIndex = rand.Next(0, enemySkills.Count);

        return enemySkills[randomIndex];
    }

    private bool TestBattleOver()
    {
        if (_playerCharacterBattle.IsDead())
        {
            // Player dead, enemy wins
            //CodeMonkey.CMDebug.TextPopupMouse("Enemy Wins!");
            //BattleOverWindow.Show_Static("Enemy Wins!");
            return true;
        }

        DestroyDeadEnemies();
        return _enemiesCharacterBattle.Count < 1;
    }

    private void DestroyDeadEnemies()
    {
        var deadIndexes = new List<int>();
        for (var i = 0; i < _enemiesCharacterBattle.Count; i++)
        {
            if (!_enemiesCharacterBattle[i].IsDead()) continue;
            deadIndexes.Add(i);
            StartCoroutine(DestroyEnemy(_enemiesGameObject[i]));
            _enemiesGameObject.RemoveAt(i);
        }

        foreach (var deadIndex in deadIndexes)
        {
            _enemiesCharacterBattle.RemoveAt(deadIndex);
        }
    }

    private IEnumerator DestroyEnemy(GameObject enemy)
    {
        yield return new WaitForSeconds(2);
        Destroy(enemy);
    }
}
