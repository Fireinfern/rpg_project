using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName="SkillBase", menuName="Skills/Basic", order=1)]
public class Skill : ScriptableObject
{
    [SerializeField] private int damageAmount = 10;

    [SerializeField] private string animationTrigger = "Attack";

    [SerializeField] private bool slideTargetPosition = true;

    public int DamageAmount => damageAmount;

    public string AnimationTrigger => animationTrigger;

    public bool SlideTargetPosition => slideTargetPosition;
    
    public Skill(int newDamageAmount, string newAnimationTrigger, bool newSlideTargetPosition)
    {
        damageAmount = newDamageAmount;
        animationTrigger = newAnimationTrigger;
        slideTargetPosition = newSlideTargetPosition;
    }
}
