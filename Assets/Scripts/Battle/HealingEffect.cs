﻿using System;
using UnityEngine;

namespace Battle
{
    public class HealingEffect : MonoBehaviour
    {
        [SerializeField] private ParticleSystem particleSystem;

        private HealthSystem healthSystem;

        private void Start()
        {
            healthSystem = GetComponent<HealthSystem>();
            if (healthSystem)
            {
                healthSystem.OnHealed += OnHealed;
            }
        }

        private void OnHealed()
        {
            particleSystem.Play(true);
        }
    }
}