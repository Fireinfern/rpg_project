using Movement;
using System;
using System.Collections;
using System.Collections.Generic;
using Battle;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBattle : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private float characterSpeed = 10.0f;
    private State state;
    private Vector3 slideTargetPosition;
    private Action onSlideComplete;
    private Action onAttackComplete;
    float attackAnimationTime = 2f;
    float deadAnimationTime = 2f;
    float attackTimer = 0f;
    float deadTimer = 0f;
    private CharacterController _characterController;
    private GameObject selectionCircleGameObject;
    private HealthSystem healthSystem;

    [SerializeField]
    private Slider healthBarSlider;

    public AudioSource attackAudioSource;
    public AudioSource damageAudioSource;

    private enum State
    {
        Idle,
        Sliding,
        Busy,
        Attack,
        Dead
    }

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        selectionCircleGameObject = transform.Find("SelectionSphere").gameObject;
        HideSelectionCircle();
        healthSystem = gameObject.AddComponent<HealthSystem>();
        healthSystem.SetMaxHealth(100);
        healthSystem.OnHealthChanged += HealthSystem_OnHealthChanged;
        state = State.Idle;
    }

    private void HealthSystem_OnHealthChanged(object sender, EventArgs e)
    {
        healthBarSlider.value = healthSystem.GetHealthPercent();
    }

    private void Update()
    {
        switch (state)
        {
            case State.Idle:
                break;
            case State.Busy:
                break;
            case State.Sliding:
                float slideSpeed = 10f;
                transform.position +=
                    slideSpeed * Time.deltaTime * (slideTargetPosition - GetPosition());

                float reachedDistance = 1f;
                if (Vector3.Distance(GetPosition(), slideTargetPosition) < reachedDistance)
                {
                    // Arrived at Slide Target Position
                    transform.position = slideTargetPosition;
                    onSlideComplete();
                }
                break;
            case State.Attack:
                attackTimer += Time.deltaTime;
                if (attackTimer > attackAnimationTime)
                {
                    attackTimer = 0f;
                    onAttackComplete();
                }
                break;
            case State.Dead:
                deadTimer += Time.deltaTime;
                if (deadTimer > deadAnimationTime)
                {
                    deadTimer = 0f;
                }
                break;
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    private void Damage(int damageAmount)
    {
        healthSystem.Damage(damageAmount);
        if (damageAudioSource!=null) damageAudioSource.Play();

        if (!healthSystem.IsDead()) return;
        animator.SetTrigger("Dead");
        state = State.Dead;
    }

    public bool IsDead()
    {
        return healthSystem.IsDead();
    }

    public void Attack(CharacterBattle targetCharacterBattle, Skill skill, Action onAttackCompleted)
    {
        var damageAmount = skill.DamageAmount;
        var attackAnimTrigger = skill.AnimationTrigger;
        if (skill.SlideTargetPosition)
        {
            var slideTargetPositionVec =
                targetCharacterBattle.GetPosition()
                + (GetPosition() - targetCharacterBattle.GetPosition()).normalized;
            var startingPosition = GetPosition();

            SlideToPosition(
                slideTargetPositionVec,
                () =>
                {
                    StartCoroutine(ShowDamagePopup(targetCharacterBattle.GetPosition(), damageAmount));
                    PerformAttack(
                        attackAnimTrigger,
                        () =>
                        {
                            targetCharacterBattle.Damage(damageAmount);
                            SlideToPosition(
                                startingPosition,
                                () =>
                                {
                                    state = State.Idle;
                                    onAttackCompleted();
                                }
                            );
                        }
                    );
                }
            );
        }
        else
        {
            PerformAttack(
                attackAnimTrigger,
                () =>
                {
                    targetCharacterBattle.Damage(damageAmount);
                    state = State.Idle;
                    onAttackCompleted();
                }
            );
        }
    }

    private void SlideToPosition(Vector3 newSlideTargetPosition, Action onSlideCompleted)
    {
        slideTargetPosition = newSlideTargetPosition;
        onSlideComplete = onSlideCompleted;
        state = State.Sliding;
    }

    private void PerformAttack(string attackAnimTrigger, Action onAttackCompleted)
    {
        animator.SetTrigger(attackAnimTrigger);
        state = State.Attack;
        onAttackComplete = onAttackCompleted;
        if (attackAudioSource!=null) attackAudioSource.Play();
    }

    private IEnumerator ShowDamagePopup(Vector3 targetPos, int damageAmount)
    {
        var position = new Vector3(targetPos.x + .5f, 1.25f, 0);
        yield return new WaitForSeconds(1);
        DamagePopup.Create(position, damageAmount);
    }

    public void HideSelectionCircle()
    {
        selectionCircleGameObject.SetActive(false);
    }

    public void ShowSelectionCircle()
    {
        selectionCircleGameObject.SetActive(true);
    }
}
