﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ExitLevelTrigger : MonoBehaviour
{

    [SerializeField] private String mapName = "";


    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
        {
            return;
        }

        SceneManager.LoadScene(mapName);
    }
}
