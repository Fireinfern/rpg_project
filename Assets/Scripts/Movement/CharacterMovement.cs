﻿using System;
using UnityEngine;

namespace Movement
{
    [RequireComponent(typeof(CharacterController))]
    public class CharacterMovement : MonoBehaviour
    {
        private CharacterController _characterController;
        public static CharacterMovement instance;
        public bool inCombat = false;
        public AudioSource footstepsAudioSource;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private float characterSpeed = 10.0f;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
        }

        private void Update()
        {
            Vector3 movementSpeed = Vector3.zero;
            if (!inCombat)
            {
                if (Input.GetKey(KeyCode.D))
                {
                    movementSpeed += Vector3.right;
                }

                if (Input.GetKey(KeyCode.A))
                {
                    movementSpeed += Vector3.left;
                }

                if (!movementSpeed.Equals(Vector3.zero))
                {
                    float currentSpeed = characterSpeed * Time.deltaTime;
                    movementSpeed *= currentSpeed;
                    _characterController.Move(movementSpeed);
                    if (footstepsAudioSource!=null && !footstepsAudioSource.isPlaying)
                    {
                        footstepsAudioSource.Play();
                    }
                }
                else
                {
                    if (footstepsAudioSource!=null) footstepsAudioSource.Stop();
                }
            }
            animator.SetFloat("Speed", movementSpeed.x);
        }
    }
}
