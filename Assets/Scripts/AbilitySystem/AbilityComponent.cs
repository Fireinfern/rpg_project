﻿using System.Collections.Generic;
using AbilitySystem.Abilities;
using UnityEngine;

namespace AbilitySystem
{
    public class AbilityComponent : MonoBehaviour
    {
        [SerializeField] private List<Attribute> attributes;

        [SerializeField] private List<BaseAbility> abilities;
        
        public List<BaseAbility> Abilities => abilities;

        public List<Attribute> Attributes => attributes;
        
        public void TryToActivateAbilityByName(string abilityName, GameObject target)
        {
            var targetAbilityComponent = target.GetComponent<AbilityComponent>();
            foreach (var ability in abilities)
            {
                ability.TryActivate(targetAbilityComponent, this);
            }
        }

        public Attribute GetAttributeByName(string nameToFind)
        {
            foreach (var attribute in attributes)
            {
                if (attribute.AttributeName.Equals(nameToFind))
                {
                    return attribute;
                }
            }
            return null;
        }
    }
}