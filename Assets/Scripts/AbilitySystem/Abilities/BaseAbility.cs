﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Events;

namespace AbilitySystem.Abilities
{
    public class BaseAbility : ScriptableObject
    {
        [SerializeField, Category("General")] private string abilityName;

        [SerializeField, Category("General"), Tooltip("If the ability ends instantly or will deffer its end")] private bool isInstantAbility;

        [Category("Events")] public UnityEvent<bool> onAbilityEnded;

        public string AbilityName => abilityName;
        
        public bool CanActivate(AbilityComponent target, AbilityComponent instigator)
        {
            return true;
        }

        public bool TryActivate(AbilityComponent target, AbilityComponent instigator)
        {
            if (!CanActivate(target, instigator))
            {
                return false;
            }
            Activate(target, instigator);
            return true;
        }

        protected void Activate(AbilityComponent target, AbilityComponent instigator)
        {
            if (isInstantAbility)
            {
                EndAbility(false);
            }
        }

        protected void EndAbility(bool isCanceled)
        {
            onAbilityEnded.Invoke(isCanceled);
        }

        public void CancelAbility()
        {
            EndAbility(true);
        }

    }
}