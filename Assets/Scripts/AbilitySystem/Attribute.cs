﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace AbilitySystem
{
    [Serializable]
    public class Attribute
    {
        [SerializeField] private string attributeName = "NONE";

        [SerializeField] private float attributeValueBase = 0.0f;

        private float _attributeValueCurrent = 0.0f;

        public UnityEvent<float, float> onAttributeChanged; 
        
        // Getters
        public string AttributeName => attributeName;

        public float AttributeValueBase => attributeValueBase;

        public float AttributeValueCurrent => _attributeValueCurrent;
    }
}