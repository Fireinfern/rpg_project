﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class AbilityButton : MonoBehaviour
    {
        [SerializeField] private TMP_Text abilityText;

        [SerializeField] private Button button;

        public TMP_Text AbilityText => abilityText;

        public Button Button => button;
    }
}