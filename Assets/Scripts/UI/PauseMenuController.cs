﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class PauseMenuController : MonoBehaviour
    {

        [SerializeField] private GameObject panelGameObject;
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                TogglePauseMenu();
            }
        }

        public void TogglePauseMenu()
        {
            panelGameObject.SetActive(!panelGameObject.activeSelf);
            Time.timeScale = !panelGameObject.activeSelf ? 1 : 0;
        }

        public void BackToMainMenu()
        {
            SceneManager.LoadScene("MainMenu");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}