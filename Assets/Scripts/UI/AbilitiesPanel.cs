﻿using AbilitySystem;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace UI
{
    public class AbilitiesPanel : MonoBehaviour
    {
        [SerializeField, Tooltip("Prefab that represents the button that will be used in the abilities")] private AbilityButton abilityButton;

        [SerializeField, Tooltip("Buttons Container Object")] private GameObject abilityButtonsContainer;

        public void LoadAbilities(AbilityComponent abilityComponent)
        {
            
        }
    }
}