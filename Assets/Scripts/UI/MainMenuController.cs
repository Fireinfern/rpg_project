﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenuController : MonoBehaviour
    {
        public void StartGame()
        {
            SceneManager.LoadScene("MainFloor");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}