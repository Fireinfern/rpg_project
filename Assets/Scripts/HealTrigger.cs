﻿using System;
using System.Collections;
using UnityEngine;

public class HealTrigger : MonoBehaviour
{
    public int healAmount = 40;

    public ParticleSystem particleSystem;

    [SerializeField] private bool needsToBeDestroyAfterUse = false;
    
    [SerializeField]
    private float cooldownTime = 20.0f;
    
    private bool _isInCooldown = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if (_isInCooldown) return;
        if (!other.CompareTag("Player")) return;
        HealthSystem healthSystem = other.GetComponent<HealthSystem>();
        if (!healthSystem) return;
        healthSystem.Heal(healAmount);
        particleSystem.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        if (needsToBeDestroyAfterUse)
        {
            Destroy(gameObject);
            return;
        }

        StartCoroutine(ManageCooldown());
    }

    IEnumerator ManageCooldown()
    {
        _isInCooldown = true;
        yield return new WaitForSeconds(cooldownTime);
        _isInCooldown = false;
        particleSystem.Play();
    }
}